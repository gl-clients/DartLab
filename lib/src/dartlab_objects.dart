import 'package:json_object/json_object.dart';

abstract class OauthCodeResponse {
    String access_token;
    String token_type;
    int expires_in;
    String refresh_token;
}

class OauthCodeResponseImpl extends JsonObject implements OauthCodeResponse {
  OauthCodeResponseImpl();

  factory OauthCodeResponseImpl.fromJsonString(string) {
    return new JsonObject.fromJsonString(string, new OauthCodeResponseImpl());
  }
}