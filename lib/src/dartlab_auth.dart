import 'dart:math';
import 'package:http/http.dart';

class DartLabAuth {

  OauthFlowType requestedOauthFlow;
  String clientId, clientSecret, redirectUri;

  /// Constructor
  /// 
  /// OauthFlowType should be one of WEB_APP, IMPLICIT OR RESOURCE_OWNER_PASSWORD_CREDENTIALS
  /// clientId can be foind in the application settings on GitLab
  /// redirectUri MUST match that provided in the API console
  /// clientSecret is optional, only required for WEB_APP auth flow
  DartLabAuth(OauthFlowType flowType, String clientId, String redirectUri, {String clientSecret}) {
    this.requestedOauthFlow = flowType;
    this.clientId = clientId;
    this.redirectUri = redirectUri;
    this.clientSecret = clientSecret;
  }

  /// Generates the auth URL required for your chosen OauthFlowType
  /// state comes from [generateState()]
  String generateAuthUrl(String state) {
    switch (requestedOauthFlow) {
      case OauthFlowType.WEB_APP:
        return _webAppUrl(state);
        break;
      case OauthFlowType.IMPLICIT:
        return _implicitUrl(state);
        break;
      case OauthFlowType.RESOURCE_OWNER_PASSWORD_CREDENTIALS:
        return _credentialUrl();
        break;
      default:
        return "Error parsing URL type";
        break;
    }
  }

  /// Generates a random state to be provided to [generateAuthUrl()]
  String generateState() {
    const seed = "abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var r = new Random();
    var buffer = new StringBuffer();
    for (var i = 0; i < seed.length; i++) {
      buffer.write(seed[r.nextInt(seed.length - 1)]);
    }
    return buffer.toString();
  }
}

String _webAppUrl(String state) {
  return "https://gitlab.com/oauth/authorize?client_id=$clientId&redirect_uri=$redirectUri&response_type=code&state=$state";
}

String _implicitUrl(String state) {
  return "https://gitlab.com/oauth/authorize?client_id=$clientId&redirectUri=$redirectUri&response_type=token&state=$state";
}

String _credentialUrl() {
  return "Resource Owner Password Credentials not yet supported.";
}

void getAccessToken(String code) {
  var url = "https://gitlab.com/oauth/token?client_id=";
}

enum OauthFlowType {
  WEB_APP,
  IMPLICIT,
  RESOURCE_OWNER_PASSWORD_CREDENTIALS
}